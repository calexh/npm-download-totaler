let username = ""
let logging
if(process.argv.length>2){
    username = process.argv.slice(2)[0]
}
let data = {}
let sum = 0
async function getApps () {
    let resString = await (await fetch(`https://npmjs.com/~${username}`)).text()
    let thisPagePackages = resString.split(`<a target="_self" href="/package/`)
    let pkgNames = thisPagePackages.map((pkg) => {
        return pkg.split('"')[0]
    }).slice(1)
    let numPkgs = 0
    resString.split(`<a id="packages" href="?activeTab=packages"`).forEach((item,idx) => {
        if(idx!=1){
            return
        } else{
            numPkgs = item.split(">")[4].split("<")[0]
        }
    })
    let pageNum = 1
    while(pkgNames.length<numPkgs){
        let more = await (await fetch(`https://npmjs.com/~${username}?page=${pageNum}`)).text()
        let morePkgs = more.split(`<a target="_self" href="/package/`).map((pkg) => {
            return pkg.split('"')[0]
        }).slice(1)
        pkgNames = [...pkgNames, ...morePkgs]
        pageNum++
    }
    for(let i=0;i<pkgNames.length;++i){
        let downloadedTimes = await (await fetch(`https://api.npmjs.org/downloads/point/1970-01-01:2038-01-19/${pkgNames[i]}`)).json();
        str = pkgNames[i] + " downloaded "+ downloadedTimes['downloads']+ " times."
        if(logging){console.log(str)}
        data[pkgNames[i]]=(downloadedTimes['downloads']==undefined?0:downloadedTimes['downloads'])
        sum+=(downloadedTimes['downloads']==undefined?0:downloadedTimes['downloads'])
    }
    if(logging){console.log("NPM User "+username+"'s Packages Download Total: "+sum.toLocaleString('fi')+" times.")}
}

module.exports.run = async function (accountName, willLog = false){
    try {
        username=accountName
        logging = willLog
        await getApps()
        return {"data":data,"total":sum}
    } catch (err){
        return err
    }
}