# author-total-downloads

## Print any User's Total Public NPM Package Downloads, Right to the Command Line.

Command Line Instructions:
1. npx author-total-downloads userToSearch
2. View output

Module Instructions:
1. npm i author-total-downloads
2. const pkg = require('author-total-downloads')
3. const data = await pkg.run('userName') <-> defaults to no logging
3. const data = await pkg.run('userName', true) <-> turn console logging on
5. returns object {data:{'package':downloads,...},total:downloads}


Features:
- No Dependencies
- Displays per package total downloads
- Works for public packages of any npm account
- Sum totals all downloads from all public packages
- Easy, convenient, single-command operation
- Easy, convenient, modular operation

Git Repo: https://gitlab.com/calexh/npm-download-totaler

Thank You for Using author-total-downloads, written by C. Alex @ YHY/SPC LLC 

We Contract Internationally!  

🇷🇺 - 🇲🇽 - 🇨🇿 - 🇫🇮 - 🇸🇪 - 🇩🇪 - 🇺🇸 

Visit **https://YHY.fi** & work with YHY/SPC LLC on your next project.