#!/usr/bin/env node
if(process.argv.length>2){
    username = process.argv.slice(2)[0]
} else {
    console.log("Missing Username")
    return
}
let args = process.argv.slice(2)
async function getApps () {
    let resString = await (await fetch(`https://npmjs.com/~${args[0]}`)).text()
    let thisPagePackages = resString.split(`<a target="_self" href="/package/`)
    let pkgNames = thisPagePackages.map((pkg) => {
        return pkg.split('"')[0]
    }).slice(1)
    let numPkgs = 0
    resString.split(`<a id="packages" href="?activeTab=packages"`).forEach((item,idx) => {
        if(idx!=1){
            return
        } else{
            numPkgs = item.split(">")[4].split("<")[0]
        }
    })
    let pageNum = 1
    while(pkgNames.length<numPkgs){
        let more = await (await fetch(`https://npmjs.com/~${args[0]}?page=${pageNum}`)).text()
        let morePkgs = more.split(`<a target="_self" href="/package/`).map((pkg) => {
            return pkg.split('"')[0]
        }).slice(1)
        pkgNames = [...pkgNames, ...morePkgs]
        pageNum++
    }
    let sum = 0
    for(let i=0;i<pkgNames.length;++i){
        let downloadedTimes = await (await fetch(`https://api.npmjs.org/downloads/point/1970-01-01:2038-01-19/${pkgNames[i]}`)).json();
        console.log(pkgNames[i] + " downloaded "+ (downloadedTimes['downloads']==undefined?0:downloadedTimes['downloads'])+ " times.")
        sum+=(downloadedTimes['downloads']==undefined?0:downloadedTimes['downloads'])
    }
    console.log("NPM User "+ args[0]+"'s Packages Download Total: "+sum.toLocaleString('fi')+" times.")
}

getApps()